package com.vuducviet.smsreceiver.model;

import com.google.gson.Gson;

import java.io.Serializable;

public class Message implements Serializable {

    private String smsid;
    private String type;
    private String id;
    private String sender;
    private String content;

    private Message() {
        this.type = "newsms";
    }

    public Message(int smsid, String id, String sender, String content) {
        this();
        this.smsid = String.valueOf(smsid);
        this.id = id;
        this.sender = sender;
        this.content = content;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
