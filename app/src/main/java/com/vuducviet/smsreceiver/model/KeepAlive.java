package com.vuducviet.smsreceiver.model;

import com.google.gson.Gson;

import java.io.Serializable;

public class KeepAlive implements Serializable {

    private String id;
    private String type;

    private KeepAlive() {
        this.type = "keepalive";
    }

    public KeepAlive(String id) {
        this();
        this.id = id;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
