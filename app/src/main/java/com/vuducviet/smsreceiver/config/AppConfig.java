package com.vuducviet.smsreceiver.config;

import android.app.Application;
import android.content.Intent;

import com.vuducviet.smsreceiver.network.CacheUtils;
import com.vuducviet.smsreceiver.network.NetworkManager;
import com.vuducviet.smsreceiver.service.RealTimeService;

public class AppConfig extends Application {

    private static AppConfig instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        CacheUtils.install(this);
        NetworkManager.install();
    }

    public static AppConfig getInstance() {
        return instance;
    }

    public void restartRealTimeService() {
        stopRealTimeService();
        startRealTimeService();
    }

    public void startRealTimeService() {
        startService(new Intent(getApplicationContext(), RealTimeService.class));
    }

    public void stopRealTimeService() {
        stopService(new Intent(getApplicationContext(), RealTimeService.class));
    }
}
