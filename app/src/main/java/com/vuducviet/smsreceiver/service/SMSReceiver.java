package com.vuducviet.smsreceiver.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;

import com.vuducviet.smsreceiver.model.Message;
import com.vuducviet.smsreceiver.network.CacheUtils;
import com.vuducviet.smsreceiver.network.NetworkManager;

import java.util.List;
import java.util.Random;

public class SMSReceiver extends BroadcastReceiver {

    private String sim;
    private Random random = new Random();

    @Override
    public void onReceive(Context context, Intent intent) {


        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Object messages[] = (Object[]) bundle.get("pdus");
            SmsMessage smsMessage[] = new SmsMessage[messages.length];
            for (int i = 0; i < messages.length; i++)
                smsMessage[i] = SmsMessage.createFromPdu((byte[]) messages[i]);
            String content = smsMessage[0].getMessageBody();
            String sender = smsMessage[0].getOriginatingAddress();
            int slot = bundle.getInt("slot", -1);
            if (slot == 0)
                sim = CacheUtils.open().getString(CacheUtils.SIM_1);
            else
                sim = CacheUtils.open().getString(CacheUtils.SIM_2);
            List<String> urls = CacheUtils.open().getBaseUrls();
            if (urls != null && !urls.isEmpty()) {
                for (String url : urls)
                    NetworkManager.getInstance().updateNewMessage(url, new Message(
                            random.nextInt(Integer.MAX_VALUE - 1) + 1, sim, sender, content));
            }
        }
    }
}
