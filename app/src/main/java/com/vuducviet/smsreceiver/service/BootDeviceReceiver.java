package com.vuducviet.smsreceiver.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vuducviet.smsreceiver.network.CacheUtils;

public class BootDeviceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
                startServiceByAlarm(context);
            }
        }
    }

    private void startServiceByAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, RealTimeService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long startTime = System.currentTimeMillis();
        if (alarmManager != null)
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, startTime, CacheUtils.open().getTime(), pendingIntent);
    }
}
