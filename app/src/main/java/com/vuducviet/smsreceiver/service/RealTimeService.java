package com.vuducviet.smsreceiver.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.text.TextUtils;

import com.vuducviet.smsreceiver.model.KeepAlive;
import com.vuducviet.smsreceiver.network.CacheUtils;
import com.vuducviet.smsreceiver.network.NetworkManager;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class RealTimeService extends Service {

    private Timer timer;
    private String sim1, sim2;
    private List<String> urls;

    @Override
    public void onCreate() {
        super.onCreate();
        timer = new Timer();
        sim1 = CacheUtils.open().getString(CacheUtils.SIM_1);
        sim2 = CacheUtils.open().getString(CacheUtils.SIM_2);
        urls = CacheUtils.open().getBaseUrls();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (urls != null && !urls.isEmpty()) {
                    for (String url : urls) {
                        if (!TextUtils.isEmpty(sim1))
                            NetworkManager.getInstance().updateKeepAlive(url, new KeepAlive(sim1));
                        if (!TextUtils.isEmpty(sim2))
                            NetworkManager.getInstance().updateKeepAlive(url, new KeepAlive(sim2));
                    }
                }
            }
        }, 0, CacheUtils.open().getTime());

        try {
            PowerManager mgr = (PowerManager) getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SMSReceiverWake");
            wakeLock.acquire();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null)
            timer.cancel();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
