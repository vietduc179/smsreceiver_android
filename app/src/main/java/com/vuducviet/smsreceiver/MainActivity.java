package com.vuducviet.smsreceiver;

import android.Manifest;
import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.vuducviet.smsreceiver.config.AppConfig;
import com.vuducviet.smsreceiver.helper.TelephonyInfo;
import com.vuducviet.smsreceiver.network.CacheUtils;
import com.vuducviet.smsreceiver.service.SMSReceiver;

import java.util.ArrayList;
import java.util.List;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends Activity implements View.OnClickListener {

    EditText editText1, editText2, editText3, editTextSim1, editTextSim2, editTextTime;

    // Filter
    SMSReceiver receiver = null;
    IntentFilter filter = null;
    List<String> urls = new ArrayList<>();

    TelephonyInfo telephonyInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText1 = findViewById(R.id.edit_text_1);
        editText2 = findViewById(R.id.edit_text_2);
        editText3 = findViewById(R.id.edit_text_3);
        editTextSim1 = findViewById(R.id.edit_text_sim_1);
        editTextSim2 = findViewById(R.id.edit_text_sim_2);
        editTextTime = findViewById(R.id.edit_text_time);
        findViewById(R.id.button).setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        editTextSim1.setText(CacheUtils.open().getString(CacheUtils.SIM_1));
        // Check dual sim
        telephonyInfo = TelephonyInfo.getInstance(this);
        if (telephonyInfo != null && telephonyInfo.isDualSIM()) {
            editTextSim2.setVisibility(View.VISIBLE);
            editTextSim2.setText(CacheUtils.open().getString(CacheUtils.SIM_2));
        }
        List<String> urls = CacheUtils.open().getBaseUrls();
        if (urls != null && !urls.isEmpty()) {
            for (int i = 0; i < urls.size(); i++) {
                if (i == 0)
                    editText1.setText(urls.get(i));
                else if (i == 1)
                    editText2.setText(urls.get(i));
                else if (i == 2)
                    editText3.setText(urls.get(i));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @NeedsPermission({android.Manifest.permission.RECEIVE_SMS,
            android.Manifest.permission.SEND_SMS,
            Manifest.permission.WAKE_LOCK,
            android.Manifest.permission.READ_PHONE_STATE})
    void requirePermission() {
        try {
            filter = new IntentFilter();
            filter.addAction("android.provider.Telephony.SMS_RECEIVED");
            receiver = new SMSReceiver();
            registerReceiver(receiver, filter);
            AppConfig.getInstance().restartRealTimeService();
            Toast.makeText(MainActivity.this, "Đã bật service", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        urls.clear();
        if (!TextUtils.isEmpty(editText1.getText().toString()))
            urls.add(editText1.getText().toString());
        if (!TextUtils.isEmpty(editText2.getText().toString()))
            urls.add(editText2.getText().toString());
        if (!TextUtils.isEmpty(editText3.getText().toString()))
            urls.add(editText3.getText().toString());

        // SIM 1
        CacheUtils.open().setBaseUrls(urls);
        if (TextUtils.isEmpty(editTextSim1.getText().toString())) {
            Toast.makeText(MainActivity.this, "Vui lòng điền số điện thoại của bạn. ", Toast.LENGTH_LONG).show();
            return;
        }
        CacheUtils.open().putToCache(CacheUtils.SIM_1, editTextSim1.getText().toString());

        // SIM 2
        if (editTextSim2.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(editTextSim2.getText().toString()))
                Toast.makeText(MainActivity.this, "Vui lòng điền số điện thoại của bạn. ", Toast.LENGTH_LONG).show();
            else
                CacheUtils.open().putToCache(CacheUtils.SIM_2, editTextSim2.getText().toString());
        }
        checkTime();
        MainActivityPermissionsDispatcher.requirePermissionWithPermissionCheck(this);
    }

    private void checkTime() {
        int time = -1;
        try {
            time = Integer.parseInt(editTextTime.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (time < 0)
            time = 10000;
        CacheUtils.open().putToCache(CacheUtils.TIME, time);
    }
}