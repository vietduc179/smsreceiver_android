package com.vuducviet.smsreceiver.network;

import android.support.annotation.NonNull;
import android.util.Log;

import com.vuducviet.smsreceiver.model.KeepAlive;
import com.vuducviet.smsreceiver.model.Message;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public final class NetworkManager {

    private static final String TAG = NetworkManager.class.getSimpleName();

    private static volatile NetworkManager mInstance = null;
    private NetworkInterface service = null;

    private NetworkManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.google.com")
                .addConverterFactory(NetworkProvider.requireGson())
                .client(NetworkProvider.requireHttpClient())
                .build();
        if (service == null)
            service = retrofit.create(NetworkInterface.class);
    }

    public static void install() {
        if (mInstance == null)
            mInstance = new NetworkManager();
    }

    public static NetworkManager getInstance() {
        if (mInstance == null)
            synchronized (NetworkManager.class) {
                if (mInstance == null)
                    mInstance = new NetworkManager();
            }
        return mInstance;
    }

    public void updateNewMessage(String url, Message message) {
        Call<ResponseBody> call = service.updateNewMessage(url, message);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.e(TAG + ":updateNewMessage ", response.toString());
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG + ":updateNewMessage ", t.getMessage());
            }
        });
    }

    public void updateKeepAlive(String url, KeepAlive message) {
        Call<ResponseBody> call = service.updateKeepAlive(url, message);
        try {
            call.execute();
            Log.e(TAG, "updateKeepAlive: OK");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "updateKeepAlive: " + e.getMessage());
        }
    }
}
