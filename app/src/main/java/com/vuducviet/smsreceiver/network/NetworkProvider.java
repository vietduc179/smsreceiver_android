package com.vuducviet.smsreceiver.network;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.converter.gson.GsonConverterFactory;

final class NetworkProvider {

    static OkHttpClient requireHttpClient() {
        OkHttpClient.Builder builder = requireHttpClientBuilder();
        builder.readTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .connectTimeout(5, TimeUnit.SECONDS);
        return builder.build();
    }

    static GsonConverterFactory requireGson() {
        return GsonConverterFactory.create(new GsonBuilder()
                .create());
    }

    private static OkHttpClient.Builder requireHttpClientBuilder() {
        return new OkHttpClient.Builder();
    }

}
