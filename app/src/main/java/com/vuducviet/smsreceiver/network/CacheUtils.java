package com.vuducviet.smsreceiver.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

public final class CacheUtils {

    private static final String TAG = CacheUtils.class.getSimpleName();
    private static String BASE_URL = TAG + "_BASE_URL";
    private static String TOTAL_SIZE = TAG + "_TOTAL_SIZE";

    public static String SIM_1 = TAG + "_SIM_1";
    public static String SIM_2 = TAG + "_SIM_2";
    public static String TIME = TAG + "TIME";

    private static CacheUtils mInstance;
    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;

    private CacheUtils(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
    }

    public static CacheUtils install(Context context) {
        if (mInstance == null)
            mInstance = new CacheUtils(context);
        return mInstance;
    }

    public static CacheUtils open() {
        return mInstance;
    }

    public String getString(String key) {
        if (preferences != null && preferences.contains(key))
            return preferences.getString(key, "");
        return "";
    }

    public <T> void putToCache(String key, T value) {
        if (preferences != null && editor != null) {
            if (value instanceof Integer)
                editor.putInt(key, (Integer) value);
            else if (value instanceof Boolean)
                editor.putBoolean(key, (Boolean) value);
            else if (value instanceof String)
                editor.putString(key, (String) value);
            else if (value instanceof List)
                editor.putString(key, value.toString());
            editor.apply();
        }
    }

    public int getInt(String key) {
        if (preferences.contains(key))
            return preferences.getInt(key, -1);
        return -1;
    }

    public int getTime() {
        int time = -1;
        if (preferences.contains(TIME))
            time = preferences.getInt(TIME, -1);
        if (time < 0)
            return 10000;
        else
            return time * 1000;
    }

    public void setBaseUrls(List<String> baseUrls) {
        if (baseUrls != null && !baseUrls.isEmpty()) {
            for (int i = 0; i < baseUrls.size(); i++)
                putToCache(String.format("%s_%s", BASE_URL, i + ""), baseUrls.get(i));
            putToCache(TOTAL_SIZE, baseUrls.size());
        }
    }

    public List<String> getBaseUrls() {
        List<String> res = new ArrayList<>();
        int total = getInt(TOTAL_SIZE);
        if (total > 0)
            for (int i = 0; i < total; i++)
                res.add(getString(String.format("%s_%s", BASE_URL, i + "")));
        return res;
    }
}