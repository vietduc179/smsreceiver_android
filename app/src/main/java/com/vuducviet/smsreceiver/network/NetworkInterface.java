package com.vuducviet.smsreceiver.network;

import com.vuducviet.smsreceiver.model.KeepAlive;
import com.vuducviet.smsreceiver.model.Message;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface NetworkInterface {

    @POST
    Call<ResponseBody> updateNewMessage(@Url String url, @Body Message message);

    @POST
    Call<ResponseBody> updateKeepAlive(@Url String url, @Body KeepAlive message);

}